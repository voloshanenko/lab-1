import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);



        System.out.println("Введіть операцію (+, -, *, /, c - конвертація, к - корені рівняння):");
        char operator = scanner.next().charAt(0);

        if (operator == 'c') {
            System.out.println("Введіть перше число:");
            double num1 = scanner.nextDouble();
            System.out.println("Введіть одиниці вимірювання (b, kb, mb):");
            String unit = scanner.next().toLowerCase();

            System.out.println("Введіть значення:");
            double value = scanner.nextDouble();

            double result;
            switch (unit) {
                case "b":
                    result = convertBytes(num1, value);
                    break;
                case "kb":
                    result = convertKilobytes(num1, value);
                    break;
                case "mb":
                    result = convertMegabytes(num1, value);
                    break;
                default:
                    System.out.println("Невідомі одиниці вимірювання");
                    return;
            }

            System.out.println("Результат конвертації: " + result);
            return;
        }
        else if(operator == 'к'){
            System.out.print("a: ");
            double a = scanner.nextDouble();
            System.out.print("b: ");
            double b = scanner.nextDouble();
            System.out.print("c: ");
            double c = scanner.nextDouble();
            double discriminant = b * b - 4 * a * c;

            if (discriminant < 0) {
                throw new ArithmeticException("Error: Quadratic equation has no real roots.");
            }

            if (discriminant == 0) {
                double root = -b / (2 * a);
                System.out.print("x = " + root);
            }
            else if (discriminant > 0){
                double root1 = (-b + Math.sqrt(discriminant)) / (2 * a);
                double root2 = (-b - Math.sqrt(discriminant)) / (2 * a);
                System.out.print("x1 = " + root1 + "\nx2 = " + root2);
            }
            return;
        }

        System.out.println("Введіть перше число:");
        double num1 = scanner.nextDouble();

        System.out.println("Введіть друге число:");
        double num2 = scanner.nextDouble();

        double result;

        switch (operator) {
            case '+':
                result = num1 + num2;
                break;
            case '-':
                result = num1 - num2;
                break;
            case '*':
                result = num1 * num2;
                break;
            case '/':
                if (num2 == 0) {
                    System.out.println("Помилка: Ділення на нуль");
                    return;
                }
                result = num1 / num2;
                break;
            default:
                System.out.println("Невідома операція");
                return;
        }

        System.out.println("Результат: " + result);
    }

    private static double convertBytes(double num1, double value) {
        return num1 * value;
    }

    private static double convertKilobytes(double num1, double value) {
        return num1 * (value * 1024);
    }

    private static double convertMegabytes(double num1, double value) {
        return num1 * (value * 1024 * 1024);
    }
}
