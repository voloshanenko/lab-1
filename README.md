<div style="text-align: center;">

# Міністерство освіти і науни України Харківський національний економічний університет ім. С.Кузнеця Кафедра інформаційних систем

 **Звіт<br>З лабораторної роботи №1 <br>з навчальної дисципліни «Програмування для мобільних пристроїв»**

</div>

<div style="text-align: right;">

Виконав:<br>Студент 4 курсу групи 6.04.122.010.21.2<br>факультету ІТ<br>Волошаненко Д.


</div>

<div style="text-align: right;">

Перевірив:<br>доц. Поляков А. О.


</div>
<div style="text-align: center;">

м. Харків<br>2024



</div>
Практична частина виконання лабораторної роботи

## learngitbranching (виконання)<br>Злиття віток в Git

# level 1
![Скриншот 1](img/Commit/1.jpg)
- git commit -m "First commit"
- git commit -m "Second commit"
# level 2
![Скриншот 2](img/Commit/photo_2024-03-21_15-47-07.jpg)
- git branch bugFix
- git checkout bugFix

# level 3
![Скриншот 2](img/Commit/photo_2024-03-21_15-47-44.jpg)
- git branch bugFix
- git checkout bugFix
- git commit -m "First commit"
- git checkout main
- git commit -m "Second commit"
- git merge bugFix

# level 4
![Скриншот 2](img/Commit/photo_2024-03-21_15-47-52.jpg)
- git branch bugFix
- git checkout bugFix
- git commit -m "First commit"
- git checkout main
- git commit -m "Second commit"
- git checkout bugFix
- git rebase main

## detached HEAD

 # level 1
![Скриншот 2](img/Head/photo_2024-03-21_15-48-01.jpg)
- git checkout C4

 # level 2
![Скриншот 2](img/Head/photo_2024-03-21_15-48-12.jpg)
- git checkout bugFix^

 # level 3
![Скриншот 2](img/Head/photo_2024-03-21_15-48-17.jpg)
- git checkout C1
- git branch -f main C6
- git branch -f bugFix bugFix~3

 # level 4
![Скриншот 2](img/Head/photo_2024-03-21_15-48-23.jpg)
- git reset HEAD~1
- git checkout pushed
- git revert HEAD

## Cherry-pick

 # level 1
![Скриншот 2](img/CherryPick/photo_2024-03-21_15-48-32.jpg)
- git cherry-pick C3 C4 C7 

![Скриншот 2](img/CherryPick/photo_2024-03-21_15-48-39.jpg)
Потім виділіть та переставте елементи в інтерактивному вікні Learn Git Branching таким чином:
 # level 2
![Скриншот 2](img/CherryPick/photo_2024-03-21_15-48-43.jpg)
- git rebase -i HEAD~4

## One Commit

 # level 1
![Скриншот 2](img/OneCommit/photo_2024-03-21_15-48-56.jpg)
- git checkout main
- git cherry-pick C4
 # level 2
![Скриншот 2](img/OneCommit/photo_2024-03-21_15-49-01.jpg)
- git rebase -i main
- git commit --amend -m "an updated commit message"
- git rebase -i main
- git branch -f main caption
 # level 3
![Скриншот 2](img/OneCommit/photo_2024-03-21_15-49-06.jpg)
- git checkout main
- git cherry-pick C2
- git commit --amend -m "an updated commit message"
- git cherry-pick caption
 # level 4
![Скриншот 2](img/OneCommit/photo_2024-03-21_15-49-12.jpg)
- git checkout C2
- git tag v1 C2
- git tag v0 C1
 # level 5
![Скриншот 2](img/OneCommit/photo_2024-03-21_15-49-16.jpg)
- git describe main
- git describe side
- git describe bugFixgit
- commit -m "Finish the task"

## Rebase over 9000
 # level 1
![Скриншот 2](img/Rebase/photo_2024-03-21_15-49-25.jpg)
- git rebase main bugFix
- git rebase bugFix side
- git rebase side another
- git rebase another main
 # level 2
![Скриншот 2](img/Rebase/photo_2024-03-21_15-49-32.jpg)
- git branch bugWork HEAD~^2~
 # level 3
![Скриншот 2](img/Rebase/photo_2024-03-21_15-49-38.jpg)
- git checkout one
- git cherry-pick C4 C3 C2
- git checkout two
- git cherry-pick C5 C4 C3 C2
- git branch -f three C2

## Push & Pull
 # level 1
![Скриншот 2](img/Push/photo_2024-03-21_15-52-20.jpg)
- git clone
 # level 2
![Скриншот 2](img/Push/photo_2024-03-21_15-52-27.jpg)
- git commit -m "First commit"
- git checkout o/main
- git commit -m "Second commit"
 # level 3
![Скриншот 2](img/Push/photo_2024-03-21_15-52-32.jpg)
- git fetch
 # level 4
![Скриншот 2](img/Push/photo_2024-03-21_15-52-37.jpg)
- git pull
 # level 5
![Скриншот 2](img/Push/photo_2024-03-21_15-52-54.jpg)
- git clone
- git fakeTeamwork main 2
- git commit -m "My commit"
- git pull
 # level 6
![Скриншот 2](img/Push/photo_2024-03-21_15-53-01.jpg)
- git commit -m "First commit"
- git commit -m "Second commit"
- git push
 # level 7
![Скриншот 2](img/Push/photo_2024-03-21_15-53-06.jpg)
- git clone
- git fakeTeamwork
- git commit -m "My commit"
- git pull --rebase
- git push
 # level 8
![Скриншот 2](img/Push/photo_2024-03-21_15-53-11.jpg)
- git reset --hard o/main
- git checkout -b feature C2
- git push origin feature

## Push master

 # level 1
![Скриншот 2](img/Origin/photo_2024-03-21_15-53-46.jpg)
- git rebase side1 side2
- git rebase side2 side3
- git rebase side3 main
- git pull --rebase
- git push
 # level 2
![Скриншот 2](img/Origin/photo_2024-03-21_15-53-53.jpg)
- git checkout main
- git pull
- git merge side1
- git merge side2
- git merge side3
- git push
 # level 3
![Скриншот 2](img/Origin/photo_2024-03-21_15-53-59.jpg)
- git checkout -b side o/main
- git commit -m "My commit"
- git pull --rebase
- git push
 # level 4
![Скриншот 2](img/Origin/photo_2024-03-21_15-54-03.jpg)
- git push origin main
- git push origin foo
 # level 5
![Скриншот 2](img/Origin/photo_2024-03-21_15-54-08.jpg)
- git push origin main~1:foo
- git push origin foo:main
 # level 6
![Скриншот 2](img/Origin/photo_2024-03-21_15-54-13.jpg)
- git fetch origin main~1:foo
- git fetch origin foo:main
- git checkout foo
- git merge main
 # level 7
![Скриншот 2](img/Origin/photo_2024-03-21_15-54-18.jpg)
- git push origin :foo
- git fetch origin :bar
 # level 8
![Скриншот 2](img/Origin/photo_2024-03-21_15-54-23.jpg)
- git pull origin bar:foo
- git pull origin main:side

## Final 
![Скриншот 2](img/Fin2.jpg)
![Скриншот 2](img/Fin1.jpg)
### Скріншот виповнених завдань 

![Скриншот 2](img/Group_18.png)

**Висновок**<br>Висновок: Під час виконання лабораторної роботи я навчився налагоджувати оточення Java-розробника.
Вивчити основи використання системи GIT.


