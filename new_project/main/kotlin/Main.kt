import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Консольний калькулятор")
    println("Оберіть режим:")
    println("1: Арифметичні операції (+, -, *, /)")
    println("2: Перетворення одиниць інформації (байт, кілобайт, мегабайт)")

    val mode = scanner.nextInt()

    when (mode) {
        1 -> performArithmeticOperations(scanner)
        2 -> convertInformationUnits(scanner)
        else -> println("Невірний вибір")
    }
}

fun performArithmeticOperations(scanner: Scanner) {
    println("Оберіть операцію (+, -, *, /):")
    val operation = scanner.next()

    println("Введіть перше число:")
    val num1 = scanner.nextDouble()

    println("Введіть друге число:")
    val num2 = scanner.nextDouble()

    val result = when (operation) {
        "+" -> num1 + num2
        "-" -> num1 - num2
        "*" -> num1 * num2
        "/" -> if (num2 != 0.0) num1 / num2 else {
            println("Помилка: ділення на нуль!")
            return
        }
        else -> {
            println("Неправильна операція")
            return
        }
    }

    println("Результат: $result")
}

fun convertInformationUnits(scanner: Scanner) {
    println("Введіть кількість в байтах:")
    val bytes = scanner.nextDouble()

    println("Оберіть одиницю для перетворення (KB, MB):")
    val unit = scanner.next().toLowerCase()

    val result = when (unit) {
        "kb" -> bytes / 1024
        "mb" -> bytes / (1024 * 1024)
        "bytes" -> bytes
        else -> {
            println("Невірна одиниця виміру")
            return
        }
    }

    println("Результат: $result $unit")
}
